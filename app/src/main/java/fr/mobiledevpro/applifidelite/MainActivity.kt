package fr.mobiledevpro.applifidelite

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.appsflyer.AppsFlyerLib
import com.reach5.identity.sdk.core.ReachFive
import com.reach5.identity.sdk.core.models.SdkConfig
import com.reach5.identity.sdk.facebook.FacebookProvider
import com.reach5.identity.sdk.webview.WebViewProvider
import fr.mobiledevpro.applifidelite.auth_frag.LoginFragment
import io.didomi.sdk.Didomi

class MainActivity : AppCompatActivity() {
    private val loginFragment = LoginFragment()

    private lateinit var client: ReachFive

    val DOMAIN    = "gifi-test.reach5.net"
    val CLIENT_ID = "9nR1wiIgELLXpSYKcb2D"
    val SCHEME    = "reachfive://9nR1wiIgELLXpSYKcb2D/callback"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if(savedInstanceState == null){
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.auth_fragment_container, loginFragment)
            transaction.commit()
        }

        val sdkConfig = SdkConfig(domain = DOMAIN, clientId = CLIENT_ID, scheme = SCHEME)

        // The list of the social providers needed by your application
        val providersCreators = listOf(WebViewProvider())
        client = ReachFive(
            sdkConfig = sdkConfig,
            providersCreators = providersCreators,
            // Your Android activity
            activity = this
        )
        setContentView(R.layout.activity_main)

        //Didomi.getInstance().setupUI(this)
    }
}