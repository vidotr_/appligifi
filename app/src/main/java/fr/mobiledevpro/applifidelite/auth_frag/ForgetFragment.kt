package fr.mobiledevpro.applifidelite.auth_frag

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import fr.mobiledevpro.applifidelite.R
import fr.mobiledevpro.applifidelite.databinding.FragmentForgetBinding

class ForgetFragment : Fragment() {
    private var _binding: FragmentForgetBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentForgetBinding.inflate(inflater, container, false)

        binding.noAccount.setOnClickListener {
            val signupFragment = SignupFragment()
            val transaction = activity?.supportFragmentManager?.beginTransaction()
            transaction?.replace(R.id.auth_fragment_container, signupFragment)
            transaction?.addToBackStack(null)
            transaction?.commit()
        }

        return binding.root
    }
}