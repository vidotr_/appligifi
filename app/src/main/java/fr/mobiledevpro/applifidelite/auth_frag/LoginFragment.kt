package fr.mobiledevpro.applifidelite.auth_frag

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.reach5.identity.sdk.core.ReachFive
import com.reach5.identity.sdk.core.models.SdkConfig
import com.reach5.identity.sdk.webview.WebViewProvider
import fr.mobiledevpro.applifidelite.R
import fr.mobiledevpro.applifidelite.SwipeActivity
import fr.mobiledevpro.applifidelite.databinding.FragmentLoginBinding

class LoginFragment : Fragment() {
    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    //private lateinit var client: ReachFive
    //val DOMAIN    = "https://gifi-test.reach5.net"
    //val CLIENT_ID = "kbdZ4mXqSrjlEn5XZ75v"
    //val SCHEME    = "reachfive://${CLIENT_ID}/callback"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)

        /*val sdkConfig = SdkConfig(domain = DOMAIN, clientId = CLIENT_ID, scheme = SCHEME)
        val providersCreators = listOf( WebViewProvider())
        client = ReachFive(
            sdkConfig = sdkConfig,
            providersCreators = providersCreators,
            // Your Android activity
            activity = requireActivity()
        )

        client.initialize({ providers ->
            // On success, do something with the retrieved list of providers registered for this ReachFive client
            // ...
            Log.d("Reach5_MainActivity", "OK")
        }, {
            // On failure, log the error message returned by the ReachFive client
            Log.d("Reach5_MainActivity", "ReachFive init ${it.message}")
        })*/

        binding.noAccount.setOnClickListener {
            val signupFragment = SignupFragment()
            val transaction = activity?.supportFragmentManager?.beginTransaction()
            transaction?.replace(R.id.auth_fragment_container, signupFragment)
            transaction?.addToBackStack(null)
            transaction?.commit()
        }

        binding.txtPswdForget.setOnClickListener {
            val forgetFragment = ForgetFragment()
            val transaction = activity?.supportFragmentManager?.beginTransaction()
            transaction?.replace(R.id.auth_fragment_container, forgetFragment)
            transaction?.addToBackStack(null)
            transaction?.commit()
        }

        binding.btnLogin.setOnClickListener {
            val i = Intent(activity, SwipeActivity::class.java)
            startActivity(i)
            activity?.finish()
        }

        return binding.root
    }
}