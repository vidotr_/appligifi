package fr.mobiledevpro.applifidelite.auth_frag

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import fr.mobiledevpro.applifidelite.R
import fr.mobiledevpro.applifidelite.databinding.FragmentSignupBinding

class SignupFragment : Fragment() {
    private var _binding: FragmentSignupBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentSignupBinding.inflate(inflater, container, false)

        binding.alreadyAccount.setOnClickListener {
            val loginFragment = LoginFragment()
            val transaction = activity?.supportFragmentManager?.beginTransaction()
            transaction?.replace(R.id.auth_fragment_container, loginFragment)
            transaction?.addToBackStack(null)
            transaction?.commit()
        }

        binding.blockPswd.setOnClickListener {
            val passwordFragment = PasswordFragment()
            val transaction = activity?.supportFragmentManager?.beginTransaction()
            transaction?.replace(R.id.auth_fragment_container, passwordFragment)
            transaction?.addToBackStack(null)
            transaction?.commit()
        }

        return binding.root
    }
}