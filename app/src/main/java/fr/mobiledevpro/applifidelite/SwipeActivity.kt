package fr.mobiledevpro.applifidelite

import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.ColorInt
import com.google.zxing.BarcodeFormat
import com.google.zxing.oned.Code128Writer
import io.didomi.sdk.Didomi

class SwipeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_swipe)

        displayBitmap("9781407189179")

        Didomi.getInstance().setupUI(this)
    }

    private fun createBarcodeBitmap(barcodeValue: String, @ColorInt barcodeColor: Int, @ColorInt backgroundColor: Int, widthPixels: Int, heightPixels: Int): Bitmap {
        val bitMatrix = Code128Writer().encode(barcodeValue, BarcodeFormat.CODE_128, widthPixels, heightPixels)
        val pixels = IntArray(bitMatrix.width * bitMatrix.height)

        for (y in 0 until bitMatrix.height) {
            val offset = y * bitMatrix.width
            for (x in 0 until bitMatrix.width) {
                pixels[offset + x] = if (bitMatrix.get(x, y)) barcodeColor else backgroundColor
            }
        }

        val bitmap = Bitmap.createBitmap(bitMatrix.width, bitMatrix.height, Bitmap.Config.ARGB_8888)

        bitmap.setPixels(pixels, 0, bitMatrix.width, 0, 0, bitMatrix.width, bitMatrix.height)

        return bitmap
    }

    private fun displayBitmap(value: String) {
        val widthPixels = resources.getDimensionPixelSize(R.dimen.width_barcode)
        val heightPixels = resources.getDimensionPixelSize(R.dimen.height_barcode)

        val codeBarre = findViewById<ImageView>(R.id.code_barre)
        val numberVip = findViewById<TextView>(R.id.number_vip)

        codeBarre.setImageBitmap(
            createBarcodeBitmap(
                barcodeValue = value,
                barcodeColor = resources.getColor(R.color.black, null),
                backgroundColor = resources.getColor(android.R.color.white, null),
                widthPixels = widthPixels,
                heightPixels = heightPixels
            )
        )

        numberVip.text = value
    }
}